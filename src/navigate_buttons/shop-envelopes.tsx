import { useNavigate } from "react-router-dom";

export const GoToShopEnvelopesButton = () => {
  const navigate = useNavigate();
  const more = () => {
    navigate(`/shop/envelopes`);
  };

  return (
    <div>
      {" "}
      <button className="shop_menu_button" onClick={more}>
        Envelopes
      </button>
      <button>remove </button>
    </div>
  );
};
