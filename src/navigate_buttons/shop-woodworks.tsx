import { useNavigate } from "react-router-dom";

export const GoToShopWoodworksButton = () => {
  const navigate = useNavigate();
  const more = () => {
    navigate(`/shop/woodworks`);
  };

  return (
    <div>
      <button className="shop_menu_button" onClick={more}>
        Woodworks
      </button>
      <button>remove </button>
    </div>
  );
};
