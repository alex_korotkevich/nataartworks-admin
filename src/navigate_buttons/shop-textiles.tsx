import { useNavigate } from "react-router-dom";

export const GoToShopTextilesButton = () => {
  const navigate = useNavigate();
  const more = () => {
    navigate(`/shop/textiles`);
  };

  return (
    <div>
      <button className="shop_menu_button" onClick={more}>
        Textiles
      </button>
      <button>remove </button>
    </div>
  );
};
