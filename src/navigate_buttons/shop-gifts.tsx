import { useNavigate } from "react-router-dom";

export const GoToShopGiftsButton = () => {
  const navigate = useNavigate();
  const more = () => {
    navigate(`/shop/gifts`);
  };

  return (
    <div>
      <button className="shop_menu_button" onClick={more}>
        Gift/Postcards
      </button>
      <button>remove </button>
    </div>
  );
};
