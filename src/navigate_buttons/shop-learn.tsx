import { useNavigate } from "react-router-dom";

export const GoToShopLearnButton = () => {
  const navigate = useNavigate();
  const more = () => {
    navigate(`/shop/learn`);
  };

  return (
    <div>
      <button className="shop_menu_button" onClick={more}>
        Learn with me
      </button>
      <button>remove </button>
    </div>
  );
};
