import { useNavigate } from "react-router-dom";

export const GoToShopArtworksButton = () => {
  const navigate = useNavigate();
  const more = () => {
    navigate(`/shop/artworks`);
  };

  return (
    <div>
      <button className="shop_menu_button" onClick={more}>
        Artworks
      </button>
      <button>remove </button>
    </div>
  );
};
