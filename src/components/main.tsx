import { FC } from "react"
import styled from "styled-components"
import { Container } from "./container/container"
import { MainMenu } from "./UI/main-menu/main-menu"

const MainStyled = styled.main({
  display: "flex",
  marginTop: "10vh",
  width: "100%",
  height: "90%",
})

export const Main: FC = () => {
  return (
    <MainStyled>
      <MainMenu />
      <Container />
    </MainStyled>
  )
}
