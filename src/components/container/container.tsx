import { FC } from "react"
import { Route, Routes } from "react-router-dom"
import {
  AboutPage,
  ArtPage,
  ArtworksPage,
  BlogPage,
  EnvelopesPage,
  GiftsPage,
  LearnPage,
  LinksPage,
  RandomiserPage,
  ShopPage,
  TextilesPage,
  WoodworksPage,
} from "../../pages"
import { CardStyled, ContainerStyled } from "./container-styles"

export const Container: FC = () => {
  return (
    <ContainerStyled>
      <CardStyled>
        <Routes>
          <Route path="/about" element={<AboutPage />}></Route>
          <Route path="/art" element={<ArtPage />}></Route>
          <Route path="/blog" element={<BlogPage />}></Route>
          <Route path="/link" element={<LinksPage />}></Route>
          <Route path="/shop" element={<ShopPage />}></Route>
          <Route path="/randomiser" element={<RandomiserPage />}></Route>

          <Route path="/shop/gifts" element={<GiftsPage />}></Route>
          <Route path="/shop/envelopes" element={<EnvelopesPage />}></Route>
          <Route path="/shop/artworks" element={<ArtworksPage />}></Route>
          <Route path="/shop/woodworks" element={<WoodworksPage />}></Route>
          <Route path="/shop/textiles" element={<TextilesPage />}></Route>
          <Route path="/shop/learn" element={<LearnPage />}></Route>
        </Routes>
      </CardStyled>
    </ContainerStyled>
  )
}
