import styled from "styled-components"

export const ContainerStyled = styled.div({
  display: "flex",
  width: "100%",
  height: "100%",
})

export const CardStyled = styled.div({
  display: "flex",
  flexDirection: "column",
  width: "100%",
  height: "100%",
  fontSize: " 200%",
  overflow: "auto",
})
