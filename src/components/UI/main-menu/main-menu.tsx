import { useContext } from "react"
import { MenuContext } from "../../../App"
import { TMenuContext } from "../../../types/menu-context"
import { Logout } from "../logout/logout"
import { MainMenuButton } from "../main-menu-button/main-menu-button"
import { MainMenuStyled, MenuContainerStyled, NoneStyled } from "./main-menu-styles"

export const MainMenu = () => {
  const menu = useContext<TMenuContext>(MenuContext)

  const Container = menu.open ? MenuContainerStyled : NoneStyled

  return (
    <Container>
      <MainMenuStyled>
        <div>
          <MainMenuButton title={"About"} route={"/about"} iconName={"fa fa-info"} />
          <MainMenuButton title={"Art"} route={"/art"} iconName={"fa fa-picture-o"} />
          <MainMenuButton title={"Blog"} route={"/blog"} iconName={"fa fa-comment-o"} />
          <MainMenuButton
            title={"Link"}
            route={"/link"}
            iconName={"fa fa-twitter on fa-square-o"}
          />
          <MainMenuButton title={"Randomiser"} route={"/randomiser"} iconName={"fa fa-star"} />
          <MainMenuButton title={"Shop"} route={"/shop"} iconName={"fa fa-gift"} />
        </div>
        <Logout />
      </MainMenuStyled>
    </Container>
  )
}
