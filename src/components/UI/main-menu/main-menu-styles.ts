import styled from "styled-components"

export const MenuContainerStyled = styled.div({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  height: " 100%",
  width: "20%",
})

export const MainMenuStyled = styled.div({
  position: "relative",
  display: "flex",
  flexDirection: "column",
  width: "100%",
  height: "100%",
  backgroundColor: "#171a1f",
  justifyContent: "space-between",
})

export const NoneStyled = styled.div`
  display: none;
  transition: transform 3s;
`
