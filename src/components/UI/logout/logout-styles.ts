import styled from "styled-components"

export const LogoutButtonStyled = styled.button`
  & {
    display: flex;
    justify-content: center;
    background-color: rgb(129, 123, 114, 0);
    color: aliceblue;
    font-family: "Times New Roman", Times, serif;
    border: none;
    font-size: 20px;
    padding: 15px;
    border-bottom: 1px solid white;
    width: 100%;
  }
  &:hover {
    background-color: #4caf50;
  }
`
