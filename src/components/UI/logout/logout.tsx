import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { ActionType } from "../../../types/token-action.enum"
import { IconStyled } from "../main-menu-button/main-menu-button-styles"
import { LogoutButtonStyled } from "./logout-styles"

export const Logout = () => {
  const dispatch = useDispatch()

  const navigate = useNavigate()

  const logout = async () => {
    // await authController.logout();
    dispatch({ type: ActionType.removeToken })
    navigate(`/`)
  }

  return (
    <>
      <LogoutButtonStyled onClick={logout}>
        <IconStyled className="fa    fa-sign-out"></IconStyled>
        Logout
      </LogoutButtonStyled>
    </>
  )
}
