import { useState } from "react"
import { categoryController } from "../../../http/category-controller"
import { TCategoryProps } from "../../../types/categoty-props"

export const UpdateCategory = (props: TCategoryProps) => {
  const [text, setText] = useState("")

  const updateCategory = async () => {
    await categoryController.update(props.category.id, text)
    setText("")
  }

  return (
    <>
      <div>
        <label>
          <input value={text} onChange={(event) => setText(event.target.value)} />
        </label>
      </div>
      <button onClick={updateCategory}>update</button>
    </>
  )
}
