import { FC } from "react"
import { useNavigate } from "react-router-dom"
import { TProps } from "../../../types"
import { IconStyled, MainMenuButtonStyled } from "./main-menu-button-styles"

export const MainMenuButton: FC<TProps> = (props) => {
  const navigate = useNavigate()
  const more = () => {
    navigate(props.route)
  }

  return (
    <MainMenuButtonStyled onClick={more}>
      <IconStyled className={`${props.iconName}`}></IconStyled>
      {props.title}
    </MainMenuButtonStyled>
  )
}
