import styled from "styled-components"

export const MainMenuButtonStyled = styled.button`
  & {
    display: flex;
    justify-content: flex-start;
    background-color: rgb(129, 123, 114, 0);
    color: aliceblue;
    font-family: "Times New Roman", Times, serif;
    border: none;
    font-size: 25px;
    padding: 15px;
    border-bottom: 1px solid white;
    width: 100%;
  }
  &:hover {
    background-color: #4caf50;
  }
  &:active {
    background-color: #4caf50;
  }
  &:focus {
    background-color: #4caf50;
  }
`

export const IconStyled = styled.div({
  width: "50%",
})
