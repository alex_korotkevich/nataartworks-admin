import { useState } from "react"
import { categoryController } from "../../../http/category-controller"
import {
  CategoryAddStyled,
  CategoryInputStyled,
} from "../../pages/category-list/category-list-styles"
import { CloseButton, DialogStyled, PublishStyled, SaveButton } from "./dialog-styles"

type TDialogProps = {
  state: any[]
  setState: Function
}

export const Dialog = (props: TDialogProps) => {
  const [published, setPublished] = useState(false)
  const [name, setName] = useState("")
  const [color, setColor] = useState("")

  const addCategory = (category: any) => {
    props.setState([...props.state, category])
  }

  const createCategory = async () => {
    try {
      if (!name) {
        return setColor("red")
      }
      setColor("")
      const id = await categoryController.create({ name, published })
      addCategory({ id, name, published })
      setName("")
      setPublished(false)
      closeForm()
    } catch (err) {
      setColor("red")
    }
  }

  function openForm() {
    const dialog = document.getElementById("myForm")
    if (dialog) {
      dialog.style.display = "block"
    }
  }

  function closeForm() {
    const dialog = document.getElementById("myForm")
    if (dialog) {
      dialog.style.display = "none"
    }
  }

  return (
    <>
      <CategoryAddStyled>
        <SaveButton onClick={() => openForm()}> Add new category</SaveButton>
      </CategoryAddStyled>
      <DialogStyled id="myForm">
        <p>Category name:</p>
        <CategoryInputStyled
          placeholder="new category"
          value={name}
          onChange={(event) => setName(event.target.value)}
        ></CategoryInputStyled>
        <div>
          <label>
            <input type="checkbox" checked={published} onChange={() => setPublished(!published)} />
            <PublishStyled>published</PublishStyled>
          </label>
        </div>
        <form method="dialog">
          <SaveButton style={{ background: color }} onClick={createCategory}>
            SAVE
          </SaveButton>
          <CloseButton onClick={closeForm}>CLOSE</CloseButton>
        </form>
      </DialogStyled>
    </>
  )
}
