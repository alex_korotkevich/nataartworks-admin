import styled from "styled-components"

export const DialogStyled = styled.dialog({
  display: "none",
  borderRadius: "10px",
  border: "none",
  boxShadow: "12px 12px 2px 1px rgba(0, 0, 255, 0.2)",
  marginLeft: "40%",
})

export const PublishStyled = styled.text({
  fontFamily: "Times New Roman, Times, serif",
  fontWeight: "bold",
})

export const SaveButton = styled.button`
  & {
    background-color: rgb(129, 123, 114);
    font-family: "Times New Roman", Times, serif;
    border-color: rgb(168, 162, 154);
    border-radius: 6px;
    letter-spacing: 1px;
    font-size: 22px;
    margin-top: 10px;
    width: 150px;
  }
  &:hover {
    background-color: #4caf50;
  }
`

export const CloseButton = styled.button`
  & {
    background-color: rgb(129, 123, 114);
    font-family: "Times New Roman", Times, serif;
    border-color: rgb(168, 162, 154);
    border-radius: 6px;
    letter-spacing: 1px;
    font-size: 22px;
    margin-top: 10px;
    width: 150px;
  }
  &:hover {
    background-color: #fd0a0a;
  }
`
