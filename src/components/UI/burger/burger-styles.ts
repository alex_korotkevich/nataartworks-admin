import styled from "styled-components"

export const BurgerButtonStyled = styled.button`
  top: 5px;
  margin-left: 1%;
  background-color: #171a1f;
  width: 50px;
  height: 50px;
  border-radius: 10px;
  border: none;
`

export const Stick = styled.div`
  width: 90%;
  height: 4px;
  background: #625454;
  border-radius: 10px;
`

export const StickOpen = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  flex-direction: column;
  width: 100%;
  height: 100%;
`

export const StickExtended = styled(Stick)`
  transform: rotate(90deg);
  width: 65%;
`

export const StickClose = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  transform: rotate(45deg);
`
