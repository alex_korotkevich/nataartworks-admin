import { FC, useContext } from "react"
import { MenuContext } from "../../../App"
import { TMenuContext } from "../../../types/menu-context"
import { BurgerButtonStyled, Stick, StickClose, StickExtended, StickOpen } from "./burger-styles"

export const BurgerButton: FC = () => {
  const menu = useContext<TMenuContext>(MenuContext)

  return (
    <BurgerButtonStyled onClick={menu.toggle}>
      {menu.open ? (
        <StickClose>
          <StickExtended />
          <Stick />
          <StickExtended />
        </StickClose>
      ) : (
        <StickOpen>
          <Stick />
          <Stick />
          <Stick />
        </StickOpen>
      )}
    </BurgerButtonStyled>
  )
}
