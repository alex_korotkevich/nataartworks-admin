import styled from "styled-components"

export const CategoryItemStyled = styled.div({
  marginLeft: "15%",
  fontFamily: "Times New Roman, Times, serif",
})

export const CategoryAddStyled = styled.div({
  display: "flex",
  paddingLeft: "5%",
  paddingTop: "2%",
  paddingBottom: "2%",
  fontFamily: "Times New Roman, Times, serif",
})

export const CategoryInputStyled = styled.input`
  & {
    height: 30px;
    border: none;
    border-radius: 5px;
    font-size: 15px;
  }
  &:focus {
    border-radius: 6px;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(29, 28, 28, 0.25);
  }
`
