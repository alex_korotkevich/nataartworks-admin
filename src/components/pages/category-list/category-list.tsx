import { useEffect, useState } from "react"
import { categoryController } from "../../../http/category-controller"
import { TCategoryProps } from "../../../types/categoty-props"
import { TCreatedCategory } from "../../../types/create-category-type"
import { Dialog } from "../../UI/dialog/dialog"
import { UpdateCategory } from "../../UI/update-category-button/update-category"
import { CategoryItemStyled } from "./category-list-styles"

export const CategoryList = () => {
  const [state, setState] = useState<TCreatedCategory[]>([])

  const load = async () => {
    const data = await categoryController.list()
    setState(data)
  }

  useEffect(() => {
    load()
  }, [])

  return (
    <>
      <ol>
        {state.map((item) => (
          <CategoryItem category={item} />
        ))}
      </ol>
      <Dialog state={state} setState={setState} />
    </>
  )
}

const CategoryItem = (props: TCategoryProps) => {
  return (
    <CategoryItemStyled>
      {props.category.name}
      <UpdateCategory category={props.category} />
    </CategoryItemStyled>
  )
}
