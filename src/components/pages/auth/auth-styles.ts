import styled from "styled-components"

export const LoginButtonStyled = styled.button`
  & {
    background-color: rgb(129, 123, 114);
    color: rgb(59, 55, 48);
    font-family: "Times New Roman", Times, serif;
    border-color: rgb(168, 162, 154);
    border-radius: 6px;
    letter-spacing: 1px;
    font-size: 30px;
  }
  &:hover {
    background-color: #4caf50;
  }
`
export const InputStyled = styled.input`
  & {
    height: 50px;
    width: 90%;
    border: none;
    font-size: 20px;
    background-color: rgb(242, 246, 248);
  }

  &:focus {
    color: #212529;
    border-radius: 6px;
    border-color: #bdbdbd;
    outline: 0;
    box-shadow: 0 0 0 0.2rem rgba(158, 158, 158, 0.25);
  }
`
export const LoginHeaderStyled = styled.div({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "20%",
  backgroundColor: " #4caf50",
  color: "aliceblue",
  fontSize: "25px",
  borderTopLeftRadius: "6px",
  borderTopRightRadius: "6px",
})

export const LoginFieldsStyled = styled.div({
  flex: "1 1 auto",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "space-around",
})

export const LoginRowStyled = styled.div({
  display: "flex",
  height: "60%",
  width: "30%",
  backgroundColor: "rgb(233, 236, 238)",
  borderRadius: "6px",
  flexDirection: "column",
})

export const LoginStyled = styled.div({
  display: "flex",
  height: "100vh",
  alignItems: "center",
  justifyContent: "center",
})
