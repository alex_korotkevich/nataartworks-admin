import { useState } from "react"
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { authController } from "../../../http/auth-controller"
import { ActionType } from "../../../types/token-action.enum"
import { Loader } from "../../loader/loader"
import {
  InputStyled,
  LoginButtonStyled,
  LoginFieldsStyled,
  LoginHeaderStyled,
  LoginRowStyled,
  LoginStyled,
} from "./auth-styles"

export const AuthPage = () => {
  const dispatch = useDispatch()

  const [login, setLogin] = useState("")
  const [password, setPassword] = useState("")
  const [color, setColor] = useState("")
  const [loading, setLoading] = useState(false)

  const navigate = useNavigate()

  const loginButton = async () => {
    try {
      if (!login || !password) {
        return setColor("red")
      }
      setColor("")
      setLoading(true)
      const payload = await authController.login(login, password)
      dispatch({ type: ActionType.setToken, payload })
      navigate(`/`)
    } catch (err) {
      setColor("red")
    } finally {
      setLoading(false)
    }
  }

  if (loading) {
    return <Loader />
  }

  return (
    <LoginStyled>
      <LoginRowStyled>
        <LoginHeaderStyled>Login Form</LoginHeaderStyled>
        <LoginFieldsStyled>
          <label>
            <InputStyled
              placeholder="Login"
              value={login}
              onChange={(event) => setLogin(event.target.value)}
            ></InputStyled>
          </label>
          <label>
            <InputStyled
              placeholder="Password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            ></InputStyled>
          </label>
          <div>
            <LoginButtonStyled onClick={loginButton} style={{ background: color }}>
              Login
            </LoginButtonStyled>
          </div>
        </LoginFieldsStyled>
      </LoginRowStyled>
    </LoginStyled>
  )
}
