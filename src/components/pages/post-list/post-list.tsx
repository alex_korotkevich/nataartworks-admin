import { useEffect, useState } from "react"
import { postController } from "../../../http/post-controller"
import { TPostItemProps } from "../../../types/post-props"

export const PostList = () => {
  const [posts, setPosts] = useState<any[]>([])

  const loadPosts = async () => {
    const newPosts = await postController.list()
    setPosts(newPosts)
  }

  useEffect(() => {
    loadPosts()
  }, [])

  return (
    <>
      <ol>
        {posts.map((item) => (
          <PostItem post={item} />
        ))}
      </ol>
    </>
  )
}

const PostItem = (props: TPostItemProps) => {
  const date = props.post.createdAt.slice(0, 10)
  return (
    <>
      <div>Title: {props.post.name}</div>
      <div>Post: {props.post.text}</div>
      <div>Created at: {date}</div>
    </>
  )
}
