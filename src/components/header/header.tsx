import { useSelector } from "react-redux"
import { TState } from "../../types"
import { BurgerButton } from "../UI/burger/burger-button"
import { HeaderCircle, HeaderStyled, HeaderText } from "./header-styles"

export const Header = () => {
  const user = useSelector<TState, any>((state) => state.user)

  return (
    <HeaderStyled>
      <BurgerButton />
      <HeaderText>Admin Nataartworks</HeaderText>
      <HeaderCircle>{user?.login?.[0].toUpperCase() || ""}</HeaderCircle>
    </HeaderStyled>
  )
}
