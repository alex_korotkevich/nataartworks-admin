import styled from "styled-components"

export const HeaderStyled = styled.header({
  position: "fixed",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  width: "100%",
  height: "10vh",
  background: "#625454",
  color: "white",
  fontSize: "200%",
})

export const HeaderText = styled.div({
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "center",
})

export const HeaderCircle = styled.div({
  display: "flex",
  marginRight: "5%",
  justifyContent: "center",
  backgroundColor: "black",
  width: "40px",
  height: "40px",
  borderRadius: "50%",
  color: "white",
})
