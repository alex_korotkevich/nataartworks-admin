export type TPostItemProps = {
  post: {
    id: string
    slug: string
    logo: null
    name: string
    text: string
    published: boolean
    createdAt: string
    updatedAt: string
    categoryId: string
  }
}
