export type TMenuContext = {
  open: boolean
  toggle: () => void
}
