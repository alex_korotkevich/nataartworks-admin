export enum ActionType {
  setToken = "SET_TOKEN",
  removeToken = "REMOVE_TOKEN",
  setUser = "SET_USER",
}
