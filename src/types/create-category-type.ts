export type TCategory = {
  //   id: string
  name: string
  published: boolean
}

export type TCreatedCategory = {
  id: string
  name: string
  published: boolean
}
