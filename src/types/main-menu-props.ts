export type TProps = {
  title: string
  route: string
  iconName: string
}
