export type TCategoryProps = {
  category: {
    id: string
    name: string
    published: boolean
  }
}
