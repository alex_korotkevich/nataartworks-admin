export const LearnPage = () => {
  return (
    <>
      <div className="shop_art">
        <div className="content_menu">
          <div className="menu">
            <img src="../public/menu.png" alt="Menu"></img>
          </div>
        </div>
        <div>
          <div className="shop_title">Shop</div>
          <div className="shop_chapter">Learn with me</div>
          <div className="gift_row"></div>
        </div>
      </div>
    </>
  );
};
