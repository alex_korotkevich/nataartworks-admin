import { MainMenu } from "../../components/UI/main-menu/main-menu"

export const GiftsPage = () => {
  return (
    <>
      <div className="admin">
        <MainMenu />

        <div className="art_row">
          <div>
            <div className="title">Shop</div>
            <div className="shop_chapter">Gift/Postcards</div>
            <button>add product</button>
          </div>
          <div className="gift_row"></div>
        </div>
      </div>
    </>
  )
}
