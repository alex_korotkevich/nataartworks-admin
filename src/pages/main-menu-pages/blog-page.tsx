import { CategoryList } from "../../components/pages/category-list/category-list"
import { PostList } from "../../components/pages/post-list/post-list"

export const BlogPage = () => {
  return (
    <>
      <div className="title">
        <div> Blog</div>
      </div>
      <CategoryList />
      <PostList />
    </>
  )
}
