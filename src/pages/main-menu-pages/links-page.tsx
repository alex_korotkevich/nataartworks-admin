export const LinksPage = () => {
  return (
    <>
      <div className="admin">
        <div className="art_row">
          <div className="title">
            <div> Links</div>
          </div>
          <div className="shop_menu">
            <a className="shop_menu_button" href="https://www.youtube.com/watch?v=yLY80CiHXcY">
              Interview for Artistcloseup
            </a>
            <a className="shop_menu_button" href="https://www.behance.net">
              Behance
            </a>
            <a className="shop_menu_button" href="https://www.patreon.com/">
              Patreon
            </a>
            <a className="shop_menu_button" href="https://t.me/nataartworks">
              Telegram
            </a>
            <a className="shop_menu_button" href="https://artcenter.by/en">
              Artcenter.by
            </a>
          </div>
        </div>
      </div>
    </>
  )
}
