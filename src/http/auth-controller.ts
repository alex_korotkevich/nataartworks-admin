// import axios from "axios"
import { baseUrl } from "../utils/config"
import axios from "./http"

class AuthController {
  public async login(login: string, password: string) {
    try {
      const res = await axios.post(`${baseUrl}/cms/login`, { login, password })
      return { admin: res.data, token: res.headers["authorization"] }
    } catch (err) {
      localStorage.removeItem("token")
      throw err
    }
  }

  //   logout = async () => {
  //     try {
  //       const token = localStorage.getItem("token");
  //       await axios.get(`${baseUrl}/logout`, {
  //         headers: { authorization: token || "" },
  //       });
  //     } catch (err) {
  //       localStorage.removeItem("token");
  //     }
  //   };
  public async me() {
    try {
      const res = await axios.get(`${baseUrl}/cms/admin/me`)
      return res.data
    } catch (err) {
      localStorage.removeItem("token")
    }
  }

  //   upload = async (req: FormData) => {
  //     try {
  //       const token = localStorage.getItem("token");
  //       await axios.put(`${baseUrl}/upload`, req, {
  //         headers: { authorization: token || "" },
  //       });
  //     } catch (err) {
  //       localStorage.removeItem("token");
  //     }
  //   };
}

export const authController = new AuthController()
