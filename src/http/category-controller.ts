import { TCategory } from "../types/create-category-type"
import axios from "./http"

const baseUrl = "/cms/category"

class CategoryController {
  public async list() {
    try {
      const res = await axios.get(baseUrl)
      return res.data
    } catch (err) {
      throw err
    }
  }

  public async create(category: TCategory) {
    try {
      const res = await axios.post(baseUrl, { ...category, enableComments: false })
      return res.data
    } catch (err) {
      console.log(err)
    }
  }

  public async update(id: string, name: string) {
    try {
      await axios.put(`${baseUrl}/${id}`, { name, published: true, enableComments: false })
    } catch (err) {
      console.log(err)
    }
  }
}

export const categoryController = new CategoryController()
