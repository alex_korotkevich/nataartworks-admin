import axios from "axios";

const checkTokenInterceptor = (config: any) => {
  const token = localStorage.getItem("token");
  if (token) {
    config.headers.authorization = `Bearer ${token}`;
  }
  return config;
};

const instance = axios.create({ baseURL: process.env.REACT_APP_BASE_URL });

instance.interceptors.request.use(checkTokenInterceptor);

export default instance;
