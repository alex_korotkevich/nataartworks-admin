import axios from "./http"

const baseUrl = "/cms/post"

class PostController {
  public async list() {
    try {
      const res = await axios.get(baseUrl)
      return res.data
    } catch (err) {
      throw err
    }
  }

  //   public async create(category: string) {
  //     try {
  //       const res = await axios.post(baseUrl, { category });
  //       return res.data;
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }
}

export const postController = new PostController()
