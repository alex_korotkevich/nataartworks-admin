import React, { createContext, useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import styled from "styled-components"
import "./App.css"
import { Header } from "./components/header/header"
import { Main } from "./components/main"
import { authController } from "./http/auth-controller"
import { AuthPage } from "./pages"
import { ActionType } from "./types"
import { TMenuContext } from "./types/menu-context"
import { TState } from "./types/token-reducer"

const MainPageStyled = styled.div({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-between",
  flexDirection: "column",
  width: "100%",
  height: "100vh",
})

export const MenuContext = createContext<TMenuContext>({ open: false, toggle: () => {} })

export const App = () => {
  const token = useSelector<TState, boolean>((state) => !!state.token)
  const dispatch = useDispatch()

  const [menu, setMenu] = useState<boolean>(false)

  const load = async () => {
    if (token) {
      const admin = await authController.me()
      dispatch({ type: ActionType.setUser, payload: { admin } })
    }
  }

  useEffect(() => {
    load()
  }, [])

  const toggle = () => {
    setMenu(!menu)
  }

  return (
    <BrowserRouter>
      {token ? (
        <MenuContext.Provider value={{ open: menu, toggle }}>
          <MainPageStyled>
            <Header />
            <Main />
          </MainPageStyled>
        </MenuContext.Provider>
      ) : (
        <AuthPage />
      )}
    </BrowserRouter>
  )
}
