import { configureStore } from "@reduxjs/toolkit"
import React from "react"
import ReactDOM from "react-dom/client"
import { Provider } from "react-redux"
import { App } from "./App"
import reportWebVitals from "./reportWebVitals"
import { ActionType } from "./types/token-action.enum"
import { TState } from "./types/token-reducer"

const defaultState: TState = {
  token: localStorage.getItem("token") || "",
  user: null,
}

const reducer = (state = defaultState, action: { type: ActionType; payload: any }) => {
  switch (action.type) {
    case ActionType.setToken:
      localStorage.setItem("token", action.payload.token)
      return { ...state, token: action.payload.token, user: action.payload.admin }
    case ActionType.removeToken:
      localStorage.removeItem("token")
      return { ...state, token: "", user: null }
    case ActionType.setUser:
      return { ...state, user: action.payload.admin }
    default:
      return state
  }
}

const store = configureStore({ reducer })

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement)
root.render(
  <Provider store={store}>
    {/* <React.StrictMode> */}
    <App />
    {/* </React.StrictMode> */}
  </Provider>
)

reportWebVitals()
